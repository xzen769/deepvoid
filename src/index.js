import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Text } from 'react-native';

const Index = () => (
  <>
    <Text>Test</Text>
    <StatusBar style="auto" />
  </>
);

export default Index;
