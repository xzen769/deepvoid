import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Text, View, Dimensions } from 'react-native';
import Index from './src/';

const { height } = Dimensions.get('window');
const style = { backgroundColor: 'green', height };

const App = () => (
  <View style={style}>
    <Index />
  </View>
);

export default App;
